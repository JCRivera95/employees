@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Employees') }}</span>
                    <a href="{{ route('employee.create') }}" class="btn btn-success float-right">{{ __('Add employee') }}</a>
                </div>
                <div class="card-body">
                    @if (session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <table id="data_table">
                        <thead>
                            <tr>
                                <th>{{ __('ID') }}</th>
                                <th>{{ __('Code') }}</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Email') }}</th>
                                <th>{{ __('Contract_type') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Updated at')}}</th>
                                <th>{{ __('Created at')}}</th>
                                <th>{{ __('Actions')}}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
    <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js" defer></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js" defer></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.jsdelivr.net/npm/luxon@1.25.0/build/global/luxon.min.js"></script>
    <script>
        var DateTime = luxon.DateTime;
        $(document).ready(function () {
            $('#data_table').DataTable({
                serverSide: true,
                ajax: {
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    url:  "{{ route('employee.datatable') }}",
                    method: 'POST'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'code', name: 'code'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'contract_type', name: 'contract_type'},
                    {
                        mData: "status",
                        mName: "status",
                        mRender: function (data, type, row) {
                            if (data == 1) {
                                return 'Active';
                            } else {
                                return 'Innactive';
                            }
                        }
                    },
                    {
                        mData: "updated_at",
                        mName: "updated_at",
                        mRender: function (data, type, row) {
                            return DateTime.fromISO(data, {zone: 'America/Monterrey'}).setLocale('es-CO').toFormat("DDD 'a las' t");
                        }
                    },
                    {
                        mData: "created_at",
                        mName: "created_at",
                        mRender: function (data, type, row) {
                            return DateTime.fromISO(data, {zone: 'America/Monterrey'}).setLocale('es-CO').toFormat("DDD 'a las' t");
                        }
                    },
                    {data: 'actions', name: 'actions', orderable: false, searchable: false},
                ]
            });
        });
    </script>
@endsection
