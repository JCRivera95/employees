<?php

namespace Database\Factories;

use App\Models\Employee;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstName(),
            'code' => Str::random(10),
            'last_name' => $this->faker->lastName(),
            'second_last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail(),
            'contract_type' => 'Contrato de prueba',
            'status' => 1,
        ];
    }
}
