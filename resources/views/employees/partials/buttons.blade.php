<div class="btn-group" role="group">
    <a href="{{ route('employee.edit', ['employee'=> $employee]) }}" class="btn btn-primary">{{ __('Edit') }}</a>
    <a href="{{ route('employee.show', ['employee'=> $employee]) }}" class="btn btn-info">{{ __('Show') }}</a>
    <form class="btn-group" action="{{ route('employee.status', ['employee'=> $employee]) }}" method="post">
      @csrf
      @method('PUT')
      <button type="submit" class="{{ $employee->status == 1 ? 'btn btn-warning' : 'btn btn-success' }}">{{ $employee->status == 1 ? __('Deactivate') : __('Activate') }}</button>
    </form>
    <form class="btn-group" action="{{ route('employee.destroy', ['employee'=> $employee]) }}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button>
    </form>
</div>
