<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{

    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     * @author Juan Carlos Rivera Ortiz
     * @created 2021-07-30
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'last_name',
        'second_last_name',
        'email',
        'contract_type',
        'status'
    ];

    /**
     * The attributes that should be cast to native types.
     * @author Juan Carlos Rivera Ortiz
     * @created 2021-07-30
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
