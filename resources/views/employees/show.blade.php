@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Employee ' . $employee->id ) }}</span>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <h6 class="font-weight-bold" for="id">{{ __('ID') }}: <span class="font-weight-normal">{{ $employee->id }}</span></h6>
                    </div>
                    <div class="form-group">
                        <h6 class="font-weight-bold" for="code">{{ __('Code') }}: <span class="font-weight-normal">{{ $employee->code }}</span></h6>
                    </div>
                    <div class="form-group">
                        <h6 class="font-weight-bold" for="name">{{ __('Name') }}: <span class="font-weight-normal">{{ $employee->name }}</span></h6>
                    </div>
                    <div class="form-group">
                        <h6 class="font-weight-bold" for="last_name">{{ __('Last name') }}: <span class="font-weight-normal">{{ $employee->last_name }}</span></h6>
                    </div>
                    <div class="form-group">
                        <h6 class="font-weight-bold" for="second_last_name">{{ __('Second last name') }}: <span class="font-weight-normal">{{ $employee->second_last_name }}</span></h6>
                    </div>
                    <div class="form-group">
                        <h6 class="font-weight-bold" for="email">{{ __('Email') }}: <span class="font-weight-normal">{{ $employee->email }}</span></h6>
                    </div>
                    <div class="form-group">
                        <h6 class="font-weight-bold" for="contract_type">{{ __('Contract Type') }}: <span class="font-weight-normal">{{ $employee->contract_type }}</span></h6>
                    </div>
                    <div class="form-group">
                        <h6 class="font-weight-bold" for="status">{{ __('Status') }}: <span class="font-weight-normal">{{ $employee->status == 1 ? 'Active' : 'Innactive'; }}</span></h6>
                    </div>
                    <a href="{{ route('employee.index') }}" class="btn btn-primary">{{ __('Go back') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
