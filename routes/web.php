<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/**
 * Employee resource.
 * @author Juan Carlos Rivera Ortiz
 * @created 2021-07-30
 */
Route::put('employee/{employee}/status', [EmployeeController::class, 'status'])->name('employee.status');
Route::post('employee/datatable', [EmployeeController::class, 'datatable'])->name('employee.datatable');
Route::resource('employee', EmployeeController::class);
