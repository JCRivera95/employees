@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ __('Add Employee') }}</span>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="mb-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('employee.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">{{ __('Name') }}</label>
                            <input type="text" value="{{ old('name') }}" class="form-control" name="name" id="name" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="last_name">{{ __('Last name') }}</label>
                            <input type="text"  value="{{ old('last_name') }}" class="form-control" name="last_name" id="last_name" placeholder="Enter last name">
                        </div>
                        <div class="form-group">
                            <label for="second_last_name">{{ __('Second last name') }}</label>
                            <input type="text" value="{{ old('second_last_name') }}" class="form-control" name="second_last_name" id="second_last_name" placeholder="Enter second last name">
                        </div>
                        <div class="form-group">
                            <label for="email">{{ __('Email') }}</label>
                            <input type="email" value="{{ old('email') }}"  class="form-control" name="email" id="email" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="contract_type">{{ __('Contract Type') }}</label>
                            <input type="text" value="{{ old('contract_type') }}" class="form-control" name="contract_type" id="contract_type" placeholder="Enter contract Type">
                        </div>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                        <a href="{{ route('employee.index') }}" class="btn btn-danger">{{ __('Cancel') }}</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection